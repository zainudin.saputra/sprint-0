import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import Const from './foo.constants';

@Entity({ name: Const.FOO_TABLE_NAME })
export class FooEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name!: string;
}
