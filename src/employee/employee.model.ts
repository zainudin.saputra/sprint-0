import mongoose, { Schema, Document, Model } from 'mongoose';
import { IEmployee } from './employee.interface';

export type EmployeeDocument = IEmployee & Document;

const EmployeeSchema: Schema = new Schema({
  name: {
    required: true,
    type: String
  },
  gender: {
    required: true,
    type: String
  },
  age: Number,
  wage: Number
});

export const EmployeeModel: Model<EmployeeDocument> = mongoose.model(
  'Employee',
  EmployeeSchema
);
