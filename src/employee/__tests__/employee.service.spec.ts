import employeeService from '../employee.service';
import employeeRepository from '../employee.repository';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

jest.mock('../employee.repository', () => ({
  get: jest.fn(),
  create: jest.fn(),
  getFirstByName: jest.fn(),
  updateOne: jest.fn(),
  deleteOne: jest.fn()
}));

describe('employeeService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getEmployees', () => {
    it('should return repository get result', async () => {
      const employeesTest = [
        {
          name: 'test'
        }
      ];
      employeeRepository.get.mockResolvedValueOnce(employeesTest);
      const employees = await employeeService.getEmployees();
      expect(employees).toEqual(employeesTest);
    });
  });

  describe('createEmployee', () => {
    it('should throw error if repository find existing employee with same name', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12,
        gender: 'm'
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce({
        name: 'test'
      });
      try {
        await employeeService.createEmployee(employeeInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_EXISTED);
      } finally {
        expect(employeeRepository.create).not.toBeCalled();
      }
    });

    it('should call repository create employee', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12,
        gender: 'male'
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce(null);
      await employeeService.createEmployee(employeeInfo);
      expect(employeeRepository.create).toBeCalledWith(employeeInfo);
    });
  });

  describe('updateEmployee', () => {
    it('should call repository update by id', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12,
        gender: 'm'
      };
      employeeRepository.updateOne.mockResolvedValueOnce(employeeInfo);
      const updatedOne = await employeeService.upateOne('123456', employeeInfo);
      expect(employeeRepository.updateOne).toBeCalledWith(
        '123456',
        employeeInfo
      );
      expect(updatedOne).toEqual(employeeInfo);
    });
  });

  describe('deleteEmployee', () => {
    it('should call repository delete by id', async () => {
      employeeRepository.deleteOne.mockResolvedValueOnce(null);
      await employeeService.deleteOne('123456');
      expect(employeeRepository.deleteOne).toBeCalledWith('123456');
    });
  });
});
