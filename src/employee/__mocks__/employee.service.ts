export const mockGetEmployees = jest.fn();
export const mockCreateEmployee = jest.fn();
export const mockUpdateOne = jest.fn();
export const mockDeleteOne = jest.fn();
const mock = jest.fn().mockImplementation(() => {
  return {
    getEmployees: mockGetEmployees,
    createEmployee: mockCreateEmployee,
    updateOne: mockUpdateOne,
    deleteOne: mockDeleteOne
  };
});

// getEmployees,
//   createEmployee,
//   upateOne,
//   deleteOne

export default mock;
