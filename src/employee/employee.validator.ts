import * as Joi from '@hapi/joi';

import userConstant from './employee.constant';

import { MongooseBase } from '../common/validators';

const EmployeeValidator = {
  name: Joi.string()
    .trim()
    .required(),
  gender: Joi.string()
    .trim()
    .required(),
  age: Joi.number()
    .min(userConstant.MIN_EMPLOYEE_AGE)
    .required(),
  wage: Joi.number()
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeValidator
})
  .required()
  .label('Response - User');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response - Users');

const CreateEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - new user');

const UpdateEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - new user');

export {
  EmployeeResponseValidator,
  CreateEmployeeRequestValidator,
  EmployeeListResponseValidator,
  EmployeeValidator,
  UpdateEmployeeRequestValidator
};
