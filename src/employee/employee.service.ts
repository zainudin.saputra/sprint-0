import { ERROR_CODE } from '../common/errors';

import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';
// import { EmployeeDocument } from './employee.model';

const getEmployees = () => {
  return employeeRepository.get();
};

const createEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return employeeRepository.create(employee);
};

const upateOne = async (id: string, employee: IEmployee) => {
  return employeeRepository.updateOne(id, employee);
};

const deleteOne = async (id: string) => {
  return employeeRepository.deleteOne(id);
};

const employeeService = {
  getEmployees,
  createEmployee,
  upateOne,
  deleteOne
};
export default employeeService;
