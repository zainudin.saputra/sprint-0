import { EmployeeModel, EmployeeDocument } from './employee.model';
import { IEmployee, IEmployeeResponse } from './employee.interface';

const EmployeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const EmployeeDocumentsToObjects = (documents: EmployeeDocument[]) =>
  documents.map(EmployeeDocumentToObject);

const get = async () => {
  const documents = await EmployeeModel.find().exec();
  return EmployeeDocumentsToObjects(documents);
};

const create = async (employee: IEmployee) => {
  const newEmployee = new EmployeeModel(employee);

  await newEmployee.save();

  return EmployeeDocumentToObject(newEmployee);
};

const getFirstByName = async (name: string) => {
  const employee = await EmployeeModel.findOne({ name }).exec();
  return employee && EmployeeDocumentToObject(employee);
};

const updateOne = async (id: string, employee: IEmployee) => {
  const updatedEmployee = await EmployeeModel.findByIdAndUpdate(id, employee, {
    new: true
  });
  return updatedEmployee && EmployeeDocumentToObject(updatedEmployee);
};

const deleteOne = async (id: string) => {
  const deletedEmployee = await EmployeeModel.findByIdAndDelete(id);
  return deletedEmployee && EmployeeDocumentToObject(deletedEmployee);
};

const employeeRepository = {
  get,
  create,
  getFirstByName,
  updateOne,
  deleteOne
};
export default employeeRepository;
