import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  CreateEmployeeRequestValidator,
  EmployeeResponseValidator,
  UpdateEmployeeRequestValidator
} from './employee.validator';
import { IEmployeeRequest } from './employee.interface';

const getEmployee: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employees',
    notes: 'Get the list of employees',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};
const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new Employee',
    notes: 'All information must valid',
    validate: {
      payload: CreateEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created.'
          }
        }
      }
    }
  }
};

const updateEmployee: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/employees/{id}',
  options: {
    description: 'Update Employee By Id',
    notes: 'All information must valid',
    validate: {
      payload: UpdateEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const updatedEmployee =
        (await employeeService.upateOne(
          hapiRequest.params.id,
          hapiRequest.payload
        )) || 'Not Found';
      return hapiResponse
        .response(updatedEmployee)
        .code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee updated.'
          }
        }
      }
    }
  }
};

const deleteEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employees/{id}',
  options: {
    description: 'Delete Employee By Id',
    notes: 'All information must valid',
    validate: {
      payload: UpdateEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const deletedEmployee =
        (await employeeService.deleteOne(hapiRequest.params.id)) || 'Not Found';
      return hapiResponse.response(deletedEmployee).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee deleted.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [
  getEmployee,
  createEmployee,
  updateEmployee,
  deleteEmployee
];
export default employeeController;
