import * as Joi from '@hapi/joi';

export const MongooseBase = {
  id: Joi.string().required(),
  __v: Joi.number(),
  _id: Joi.any()
};
