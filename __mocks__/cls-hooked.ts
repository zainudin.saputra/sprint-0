const cls = jest.genMockFromModule('cls-hooked');

const getNameSpace = () => {
  return {
    get: () => 'dummy'
  };
};

export { getNameSpace };
